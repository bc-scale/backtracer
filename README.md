# BackTracer


## Requires
- [Node JS](https://nodejs.org/en/download/)
 
    - Once installed, you must install globally [Nest](https://nestjs.com/) and [Angular](https://angular.io/) `npm install -g @nestjs/cli @angular/cli`

- [Docker](https://docs.docker.com/get-docker/) 
## Clone the project

Open a terminal in the installation folder and clone the repository
```shell
git clone --recurse-submodules https://gitlab.com/backtracer/backtracer
```

## Project installation 

### From Bash Terminal
Keep your last terminal in the same path and move into the cloned repository. Unzip the database folder and set up submodule's projects : 
```shell
cd backtracer/API-backtracer/
npm i 
cd ../Front-backtracer/
npm i
```

### With Other Terminal
Now you must unzip the docker.zip file to have this current path in you project : backtracer/docker/db where backtracer is the cloned directory.
Keep your last terminal in the same path and move into the cloned repository. Unzip the database folder and set up submodule's projects :
```shell
cd backtracer/API-backtracer/
npm i 
cd ../Front-backtracer/
npm i
```

### Configuration
To configure the application in a dev or a prod environment you must provide a `.env` file with the following informations :
```
POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_DATABASE=backtracer
POSTGRES_USER=backtracer
POSTGRES_PASSWORD=backtracer
DB_HOST_DOCKER=db
API_NAVITIA_TOKEN=XXX
``` 
This file permit you to connect to the database. If you want to use an other database, you just have to change the properties of this file.

To fill the navitia token, you must create an application and the token ➡️ [https://navitia.io/](https://navitia.io/)
## Launch the project (DEV Mode)
Go back to the root directory of the project `cd ..`

The project is divided in 3 parts 

### **The Postgres Database**
The Postgres Database runs using Docker
Before to launch the database, make sure that Docker is running.
The database has persistent content so you already have to make sur that in the root of the *backtracer* repository you have the following path `docker/db/`.

You can now run the database from the `backtracer/` dir
```shell
docker-compose up --build 
```
This command will launch the database using the persistent content inside `./docker` folder
You also can access to the IHM of the database at [localhost:5431](localhost:5431/)

the database access informations are given in the [.env file](https://gitlab.com/backtracer/backtracer/-/blob/main/.env)

### **The Front Component**
The Front part of the application can be run like others node project. Make sure to have already execute a `npm install` inside the `Front-backtracer` folder.

`docker-compose up` install the Front part. 
If you want to run the Front-Part separately : 
```
npm run start
```


### **The API Component**
The API part of the application can be run like others node project. Make sure to have already execute a `npm install` inside the `API-backtracer` folder.

`docker-compose up` install the API component. 
If you want to run the Front-Part separately : 
```
npm run start
```

## Launch the project (PROD Mode)
You must install your Postgres Database on another service.
You can simply install your database installing a portainer on a server or by adding a postgres container on `docker-compose.production.yml`. 

You can install all the containers on a server but you only have to expose the API and the Front modules. Normaly, the Postgres container must be only linked to the API.

Before to launch the database, make sure that Docker is running.
The database has persistent content so you already have to make sur that in the root of the *backtracer* repository you have the following path `docker/db/`.

You can now run the database from the `backtracer/` dir
```shell
docker-compose -f docker-compose.production.yml up --build 
```

the database access informations are given in the [.env file](https://gitlab.com/backtracer/backtracer/-/blob/main/.env)
You can access to the front using port 80 and the API using port 3000.

## Initialize Database
You can initialize your database by calling the following url once the API's up: 
```
${APIHOST}/gtfs?agency=10&calendar=10&calendarDate=10&route=500&trip=500&stop=1000&stopTime=1000&postscript=1&routeTypes=1000
```
where APIHOST is the address of the API.
This treatment can be very very VERY VERY long (more than 10 hours with a 3.6 GHz processor) but it depens on your computer performance and the limits set for your containers.